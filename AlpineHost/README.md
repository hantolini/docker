# Configuración de un host para contenedores con Alpine Linux

## Configuración Base

### Distribución:  Alpine Linux 3.11_alpha20190707  (Version Edge)

Se elige esta distribución ***edge*** porque incluye Docker v.19.03.1-ce.

Para subir a esta distribución desde la instalación, se deben habilitar los repositorios correspondientes en el archivo ```/etc/apk/repositories```

* repositorio ```community```
* repositorio ```edge/main```
* repositorio ```edge/community```

Luego hacer ```# apk update```, y ```# apk upgrade```

Se debe agregar algunos paquetes base para tener herramientas comunes en otras distribuciones:

* ```apk add shadow```
* ```apk add sudo```
* ```apk add zsh```
* ```apk add tmux```
* ```apk add vim``` (versión completa del editor VI)
* ```apk add htop```

Finalmente, se recomienda la creación de un usuario adicional para la operatoria común, con permisos para ejecutar acciones como ```root```.

* ```addgroup <username>```
* ```adduser -G <username> <username>```
* ```usermod -a -G wheel <username>```   (No olvidar habilitar el grupo wheel en ```visudo```)

### Instalación de Docker Engine

Se define instalar Python3 como interprete por defecto para el sistema, luego, se instala Docker y Docker-Compose.

* ```apl add python3```
* ```apk add docker```
* ```apk add docker-compose```
* ```rc-update add docker boot``` (Inicia el servicio en tiempo de boot)
* ```usermod -a -G docker <username>``` (Agrega permisos al usuario para ejecutar Docker)

### Instalación de Ansible para automatización de los procesos

Se realiza la instalación de Ansible usando el manejador de paquetes de Python, en lugar de ```apk```:

* ```apk add build-base```   (Herramientas de compilacion)
* ```sudo pip3 install ansible```  (con Sudo para instalar a nivel de sustema)
* ```sudo pip3 install docker```   (dependencias para controlar Docker con Ansible)
* ```sudo pip3 install docker-compose```  (dependencias para controlar Docker-Compose con Ansible)


