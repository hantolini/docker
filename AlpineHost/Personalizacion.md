# Personalización del Host Docker con Alpine Linux

## Configuración de Ahorro de Energía

### Display del Host

Para ahorrar energía eléctrica y que no se caliente el equipo configuramos que se apague el monitor luego de 1 minuto de inactividad.

En nuestro caso, el host es una Notebook. Cuando se cierra el LID, el Sistema Operativo no toma ninguna acción.

En el caso de Alpine, la configuración de los estados del monitor se realizan accediendo a los [controles de Framebuffer][1].

Podemos configurar el tiempo para el ScreenSaver / Apagado con una línea de secuencias ESC en la consola del FrameBuffer:

```# echo -e '\033[9;<x>]'```  donde ***x*** es el tiempo que debe transcurrir sin actividad para apagar el monitor.

Se puede agregar este comando en ```/etc/inittab``` para que se configure durante el tiempo de boot. Agregamos:

```tty1::sysinit:echo -e '\033[9;<x>]'```

[1]: http://www.armadeus.org/wiki/index.php?title=Framebuffer
